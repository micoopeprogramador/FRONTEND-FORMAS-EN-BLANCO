import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  // Array de plugins de Vite
  plugins: [
    react(), // Plugin para trabajar con React
  ],
  // Configuración del servidor de desarrollo
  server: {
    // Puerto en el que el servidor de desarrollo escuchará las solicitudes
    port: 5173,
    // Dirección IP en la que el servidor de desarrollo escuchará las solicitudes
    // '0.0.0.0' permite que el servidor escuche en todas las interfaces de red
    host: '0.0.0.0',
  },
});
